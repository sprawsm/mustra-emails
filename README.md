# Mustra Emails, the email builder kit

Mustra Emails is an HTML email template builder powered by Gulp and Pug.

The main features are:

*   A basic front-end templating system with layouts and partials based on Pug
*   Modular sections for different email layouts
*   SCSS stylesheets
*   CSS inliner
*   Images minifier
*   Gulp build tool and BrowserSync for live reloading
*   Send test emails via [Nodemailer][nodemailer]
*   Gulp tasks for an efficient workflow  

Mustra Emails utilises [Zurb Ink][zurb-templates] for its starter templates – however, this is not a requirement.

## Requirements

1. NPM - Node.js package manager ([install][npm]) or Yarn - Node.js dependency manager ([install][yarn-install])

## Installation

1. Install requirements

2. Clone Mustra Emails repository from GitLab:

    `$ git clone https://gitlab.com/sprawsm/mustra-emails.git`

3. Install required dependencies:

    `$ npm install` or `$ yarn install`

4. Build source files:

    `$ gulp build`

5. Start local web server and watch task with:

    `$ gulp server`

## Working with Mustra Emails

### Folder structure

* `/source` - source files (images, markup, styles, etc.)
* `/dist/dev` - built files (development version)
* `/dist/prod` - built files (production version with inlined css styles)

### Usage

`$ gulp server` - starts a local webserver on [http://localhost:3001][localhost]
`$ gulp build` - builds production ready files in *dist/production* folder.  

`$ gulp mail --template=NAME` - send a test email using your default configuration in `nodemailer.config.js`  
`$ gulp mail -t NAME` - alias for the above task  
`$ gulp mail --template=NAME --to=email@example.com --subject='Lorem Ipsum'` - send a test email with overrides  
`$ gulp clean` - empty your build directories

### Basic setup

When you clone Mustra Emails you should update some information to better suit your project needs:

- update information about your project in `package.js`
- update information in `source/markup/partials/_variables.pug`
- update your credentials and email options in `secret.js`

[nodemailer]: https://github.com/nodemailer/nodemailer
[zurb-templates]: https://foundation.zurb.com/emails/email-templates.html
[npm]: https://docs.npmjs.com/getting-started/installing-node
[yarn-install]: https://yarnpkg.com/en/docs/install
[localhost]: http://localhost:3001
