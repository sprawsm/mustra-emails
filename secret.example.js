/*
* Secret Credentials
*/
module.exports = {
    "rsync": {
        "hostname": "server hostname or IP address",
        "username": "ssh username",
        "destination": "destination path e.g. /var/www/user-name/public_html/project-name"
    },
    "mailer": {
        "service": "gmail // for supported services see https://github.com/andris9/nodemailer-wellknown#supported-services",
        "auth": {
            "user": "your@username",
            "pass": "your password"
        },
        "options": {
            "from": "Email Test <youremail@domain.com>",
            "to": "youremail@domain.com",
            "subject": "Test email - Sent by Mustra Email"
        }
    }
};
