// Global variables
var gulp                  = require('gulp');
var $                     = require('gulp-load-plugins')();

var del                   = require('del');
var colors                = require('colors');
var path                  = require('path');
var runSequence           = require('run-sequence');
var browserSync           = require('browser-sync');
var reload                = browserSync.reload;
var nodemailer            = require('nodemailer');
var fs                    = require('fs');
var argv                  = require('yargs').argv;

var secret                = fs.existsSync('./secret.js') ?
                            require('./secret.js') :
                            require('./secret.example.js');

// Global paths
var npmVendorsDir         = 'node_modules/';
var srcDir                = 'source/';
var destDir               = 'dist/';
var destDevDir            = destDir + 'dev/';
var destProdDir           = destDir + 'prod/';


// Customized error message
var errorAlert = function (error) {
    var fullMessage = 'Error in **' + error.plugin + '**:' + error.message;

    $.notify.onError({
        title: error.plugin,
        message: error.message,
        sound: 'Frog'
    })(error);

    fullMessage = colors.bgRed.white(fullMessage);

    console.log(fullMessage);
};


// Styles paths
var css = {
    src: [
        srcDir + 'scss/styles.scss',
        srcDir + 'scss/styleguide.scss'
    ],
    dest: destDevDir + 'assets/css/',
    watch: srcDir + 'scss/**/*.scss'
};


// Markup paths and options
var html = {
    src: [
        srcDir + 'markup/**/*.pug',
        '!source/markup/partials/*.pug',
        '!source/markup/styleguide/*.pug'
    ],
    srcInline: [
        destDevDir + '*.html',
        '!dist/dev/index.html',
    ],
    dest: destDevDir,
    destProd: destProdDir,
    watch: srcDir + 'markup/**/*.pug'
};


// Images paths
var images = {
    src: [
        srcDir + 'images/**/*'
    ],
    dest: destDevDir + 'assets/images/',
    destProd: destProdDir + 'assets/images/',
};


// Shared plugins options
var options = {
    plumber: {
        errorHandler: errorAlert
    },
    pug: {
        pretty: true
    },
    rsync: {
        root: destDevDir,
        hostname: secret.rsync.hostname,
        username: secret.rsync.username,
        destination: secret.rsync.destination,
        archive: true,
        incremental: true,
        recursive: true,
        compress: true,
        clean: true,
        silent: false,
        progress: false,
        command: false,
        dryrun: false,
        exclude: []
    },
    inlineCss: {
        applyStyleTags: true,
        applyLinkTags: true,
        removeStyleTags: false,
        removeLinkTags: true,
        removeHtmlSelectors: true
    },
    nodemailer: {
        transportOptions: {
          service: secret.mailer.service,
          auth: {
            user: secret.mailer.auth.user,
            pass: secret.mailer.auth.pass
          }
        },
        mailOptions: {
          from: secret.mailer.options.from,
          to: secret.mailer.options.to,
          subject: secret.mailer.options.subject
        }
    }
};



//
// Styles Tasks
//
// Compiles and autoprefixes styles
gulp.task('css', function () {
    return gulp.src(css.src)
        .pipe($.plumber(options.plumber))
        .pipe($.sass())
        .pipe(gulp.dest(css.dest))
        .pipe(browserSync.stream({match: '**/*.css'}));
});



// Markup Task
// Builds html markup
gulp.task('html', function() {
    return gulp.src(html.src)
        .pipe($.plumber(options.plumber))
        .pipe($.pug(options.pug))
        .pipe(gulp.dest(html.dest));
});



// Inline Task
// Inlines all CSS styles
gulp.task('inline', function() {
  return gulp.src(html.srcInline)
    .pipe($.inlineCss(options.inlineCss))
    .pipe(gulp.dest(html.destProd));
});



// Images Task
// Compresses and copies images
gulp.task('images', function () {
    return gulp.src(images.src)
        .pipe($.imagemin())
        .pipe(gulp.dest(images.dest))
        .pipe(gulp.dest(images.destProd));
});



// Send Test Email Task
//
// Sends test email to developer's email
gulp.task('mail', function() {
  var template = argv.template ? argv.template : (argv.t ? argv.t : null);

  if (! template) {
    return errorAlert({
        plugin: 'nodemailer',
        message: '***ERROR***: Name of template is missing. Please add --template=NAME and try again.'
    });
  }

  // Nodemailer
  var transporter = nodemailer.createTransport(options.nodemailer.transportOptions);
  var mailOptions = options.nodemailer.mailOptions;

  // Update config values
  mailOptions.to = argv.to ? argv.to : options.nodemailer.mailOptions.to;
  mailOptions.subject = argv.subject ? argv.subject : options.nodemailer.mailOptions.subject;

  // get template contents and send email
  fs.readFile(html.destProd + template + '.html', 'utf8', function(error, data) {
    if (error) {
        return errorAlert({
            plugin: 'nodemailer',
            message: error
        });
    }

    var regExp = /(\.\.)?\/?images\//g;
    mailOptions.html = data.replace(regExp, options.nodemailer.imageHost);

    // Send the email
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        return errorAlert({
            plugin: 'nodemailer',
            message: error
        });
      }

      console.log(colors.bgGreen.white('Test email for template "' + template + '" sent successfully.'));
    });
  });
});



// Clean Task
// Deletes folder with built files
gulp.task('clean', function () {
    return del(destDir);
});



// Watch Task
// Runs watcher mechanism for runing tasks on file update
gulp.task('watch', function () {
    gulp.watch(css.watch, ['css', reload]);
    gulp.watch(html.watch, ['html', reload]);
    gulp.watch(html.srcInline, ['inline']);
    gulp.watch(images.src, ['images', reload]);
});



// Watch Task
// Runs a web server with watch tasks
gulp.task('server', function () {
    browserSync({
        server: destDevDir,
        port: 3001
    });

    gulp.start('watch');
});



// Deploy Task
// Synchorizes files with the destination server (uses rsync)
gulp.task('deploy', function () {
    return gulp.src(destDevDir)
        .pipe($.rsync(options.rsync));
});



//
// Default and Utility Tasks
//
gulp.task('assets', ['css', 'images']);

gulp.task('build', function (callback) {
    runSequence('clean', ['html', 'assets'], 'inline', callback);
});

gulp.task('start', ['server']);
gulp.task('default', ['build']);
